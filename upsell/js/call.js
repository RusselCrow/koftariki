var koftarik = new Flickity('.koftarik-carousel', {
    contain: true,
    pageDots: false,
    wrapAround: true,
    imagesLoaded: true
});
var lunaMini3 = new Flickity('.moon-carousel', {
    contain: true,
    pageDots: false,
    wrapAround: true,
    imagesLoaded: true
});
var lunaMini4 = new Flickity('.constructor-carousel', {
    contain: true,
    pageDots: false,
    wrapAround: true,
    imagesLoaded: true
});

var QueryString = function () {
    var query_string = {};
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (typeof pair[0] != 'undefined'
            && typeof pair[1] != 'undefined'
            && pair[1].length > 0) {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
        }
    }
    return query_string;
}();

window.onload = function (event) {
    var query = QueryString;
    console.log('query is :', query);

    /***
    * config:{button:'',PRODUCT_ID:'',PRODUCT_COST:'',iframeID:''}
    *
    *
    */
    function sendData(config) {
        console.log('sending data');
        config.button.disabled = true;
        config.button.classList.add('disabled');
        var src = 'https://lk.favobox.ru/api/Order/Upsell?OrderId=' + query.orderId + '&Products='+config.PRODUCT_ID+';1;'+config.PRODUCT_COST+'&RedirectUrl=""';
        iframe=document.createElement('iframe');
        // {src:src,width:0,height:0,border:0,id='query1'};
        iframe.setAttribute("src", src);
        iframe.setAttribute("id", config.iframeID);
        iframe.style.width = "0px";
        iframe.style.height = "0px";
        iframe.style.border = "0px";
        var iframeLoaded=function(event){
            iframe.removeEventListener('load',iframeLoaded);
            console.log('iframe loaded', event);
            // config.button.classList.add('added');
            // config.button.innerHTML='Добавлено';
            iframe.remove();
        };
        iframe.addEventListener('load',iframeLoaded);
        document.body.appendChild(iframe);
        return false;
        // body...
    }

    // кнопка кофтарик
    var button1 = document.querySelector("#btn1");
    button1.addEventListener('click', function () {
        //определяет выбранную игрушку
        var id = 0;
        switch (koftarik.selectedIndex) {
            case 0:     // 1 картинка
                id = 5173;
                break;
            case 1:     // 2 картинка
                id = 5174;
                break;
            case 2:     // 3 картинка
                id = 5175;
                break;
            case 3:     // 4 картинка
                id = 5176;
                break;
        }
        return sendData({
            button:button1,
            PRODUCT_ID: id, //айди кофтарика
            PRODUCT_COST: 2190, //цена кофтарика
            iframeID:'query1'
        });
    });

    // кнопка луна
    var button5 = document.querySelector("#btn2");
    button5.addEventListener('click', function () {
        return sendData({
            button:button5,
            PRODUCT_ID: 4947, //айди 
            PRODUCT_COST: 1380, //цена
            iframeID:'query2'
        });
    });
    // кнопка шурик
    var button6 = document.querySelector("#btn3");
    button6.addEventListener('click', function () {
        return sendData({
            button:button6,
            PRODUCT_ID: 4948, //айди 
            PRODUCT_COST: 1280, //цена
            iframeID:'query3'
        });
    });
}
